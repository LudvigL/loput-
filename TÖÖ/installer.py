import subprocess
import sys

def install(name):
    subprocess.check_call([sys.executable, '-m', 'pip', 'install', name])
    
    
install('music21')
install('matplotlib')